2020.3:
-------
SD card "permission denied" fix               - @simonpunk
"Wrong session view" fix                      - @simonpunk
Speed improvements                            - @simonpunk
Ctrl key detection improvments                - @simonpunk
New: "zoom in - zoom out" to change font size - @simonpunk

2020.1:
-------
! Ensure /data is not encrypted, see https://forum.xda-developers.com/android/software/universal-dm-verity-forceencrypt-t3817389
* Roll back change to resolve "key not required" issue caused by encrypted data partition due to sporadic undesired consequences, such as "socket: Permission  denied" errors. 
* Migrate hardcoded "kali-armhf" rootfs name to "kalifs" symlink
* Force enabling UTF-8 mode
* All su terminals are run with global mount namespace
* Added nh-app scripts path and bin path to nh-term